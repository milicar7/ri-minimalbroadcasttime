# Minimal Broadcast Time

This project contains implementation of different algorithms for Minimal broadcast time problem that are tested on different types of graphs that can be found in folders [graphs](https://gitlab.com/milicar7/ri-minimalbroadcasttime/-/tree/main/graphs) and [graphs2](https://gitlab.com/milicar7/ri-minimalbroadcasttime/-/tree/main/graphs2)


# List of solutions


- [Brute Force solution](https://gitlab.com/milicar7/ri-minimalbroadcasttime/-/blob/main/BruteForce.ipynb)

- [Integer Linear Programming solution](https://gitlab.com/milicar7/ri-minimalbroadcasttime/-/blob/main/mbt_ilp.ipynb)

- [AM Heuristic solution](https://gitlab.com/milicar7/ri-minimalbroadcasttime/-/blob/main/AMHeuristic.ipynb)

- [Genetic algorithm](https://gitlab.com/milicar7/ri-minimalbroadcasttime/-/blob/main/mbt_ga.ipynb)

- [Iterative algorithm](https://gitlab.com/milicar7/ri-minimalbroadcasttime/-/blob/main/mbt_iterative.ipynb)

Additional information on all solutions can be found in [this](https://gitlab.com/milicar7/ri-minimalbroadcasttime/-/blob/main/Minimal_broadcast_time.pdf) pdf written in Serbian
